variable "docker_repository" { default=null }
variable "image_version" { default = "3.8.4" }


locals {
  docker_repository = "kustomize"
  maintainer = "oss@unimatrixone.io"
}
