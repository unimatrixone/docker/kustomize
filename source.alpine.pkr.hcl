

source "docker" "latest" {
  image   = "alpine:3.12"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}",
    "ENTRYPOINT [\"python\"]",
  ]
}


source "docker" "alpine" {
  image   = "alpine:3.12"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}",
    "ENTRYPOINT [\"python\"]",
  ]
}


source "docker" "gitlab" {
  image   = "alpine:3.12"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "WORKDIR ${local.runtime_home}",
    "ENTRYPOINT [\"\"]",
    "CMD []",
  ]
}
