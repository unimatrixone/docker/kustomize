bandit==1.6.2
piprot==0.9.11
pylint==2.6.0
safety==1.9.0
yamllint==1.24.2
